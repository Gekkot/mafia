package com.gekkot.mafia.model;

import com.gekkot.mafia.exception.NotEnoughPeoplesException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

class RoleDistributorTest {

    private final Set<Role> defaultAllowedRoles = Set.of(Role.CIVIL,Role.MAFIA,Role.OFFICER,Role.MEDIC);

    @Test
    void generateRoles() throws NotEnoughPeoplesException {

        //GIVEN
        int count = 6;
        RoleDistributor roleDistributor = new RoleDistributor();

        //ACT
        List<Role> roles = roleDistributor.generateRoles(count, defaultAllowedRoles);

        //THEN
        Assertions.assertNotNull(roles);
        Assertions.assertEquals(count, roles.size());

        Assertions.assertEquals(2, roles.stream().filter(it -> it.equals(Role.MAFIA)).count());
    }

    @Test
    void generateCivilRoles() {

        //GIVEN
        int count = 5;
        RoleDistributor roleDistributor = new RoleDistributor();

        //ACT
        List<Role> roles = roleDistributor.generateCivilRoles(count, defaultAllowedRoles);

        //THEN
        Assertions.assertNotNull(roles);
        Assertions.assertEquals(count, roles.size());
        Assertions.assertEquals(1, roles.stream().filter(it -> it.equals(Role.OFFICER)).count());
    }
}