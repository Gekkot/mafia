package com.gekkot.mafia.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RoleTest {

    @Test
    void repeat() {
        //GIVEN
        Role role = Role.CIVIL;
        int count = 5;

        //ACT
        Role[] repeat = role.repeat(count);

        //THEN
        assertEquals(count, repeat.length);
        for(Role repeated : repeat){
            assertEquals(role, repeated);
        }
    }
}