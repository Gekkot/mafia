package com.gekkot.mafia.model;

import com.gekkot.mafia.exception.NotEnoughPeoplesException;

import java.util.*;

import static com.gekkot.mafia.model.Role.*;

public class RoleDistributor {

    //one mafia for every MAFIA_COEF players
    private final static int MAFIA_COEF = 3;


    Role selectOne(Role... role) {

        return Arrays.stream(role)
                .min((o1, o2) -> new Random().nextInt(-1, 2))
                .orElse(null);
    }

    List<Role> generateRoles(int count, Set<Role> allowedRoles) throws NotEnoughPeoplesException {
        if (count < 4) {
            throw new NotEnoughPeoplesException();
        }
        List<Role> roles = new ArrayList<>(count);
        int mafiaCount = count / MAFIA_COEF;
        roles.addAll(MAFIA.repeatAsList(mafiaCount));
        List<Role> civils = generateCivilRoles(count - mafiaCount, allowedRoles);
        roles.addAll(civils);
        return roles;
    }

    List<Role> generateCivilRoles(int civilCount, Set<Role> allowedRoles) {

        List<Role> civils = new ArrayList<>();
        if (civilCount == 4) {
            civils.add(selectOne(OFFICER, MEDIC));
        }
        if (civilCount > 4) {
            civils.add(OFFICER);
        }
        if (civilCount > 6) {
            if(allowedRoles.contains(MANIAC)) {
                civils.add(MANIAC);
            }
        }

        if (civilCount > 8) {
            if(allowedRoles.contains(LADY)) {
                civils.add(LADY);
            }
        }
        civils.addAll(CIVIL.repeatAsList(civilCount - civils.size()));

        return civils;

    }
}
