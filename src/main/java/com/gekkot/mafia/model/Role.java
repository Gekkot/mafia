package com.gekkot.mafia.model;

import java.util.Collections;
import java.util.List;

public enum Role {
    MAFIA,
    OFFICER,
    DETECTIVE,
    MEDIC,
    LADY,
    MANIAC,
    CIVIL,

    ;

    Role[] repeat(int count){
        return Collections.nCopies(count, this).toArray(new Role[count]);
    }

    List<Role> repeatAsList(int count){
        return Collections.nCopies(count, this).stream().toList();
    }
}
