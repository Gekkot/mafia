package com.gekkot.mafia.model;

import java.util.Set;

public class GamePreset {

    Set<Role> allowedRoles = Set.of(Role.values());

    boolean showRoleAfterDeath = true;
}
