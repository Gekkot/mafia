package com.gekkot.mafia.exception;

public class NotEnoughPeoplesException extends Exception {
    public NotEnoughPeoplesException() {
        super("not enough peoples");
    }
}
